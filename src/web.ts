import { WebPlugin } from '@capacitor/core';

import type { OpenFolderPlugin } from './definitions';

export class OpenFolderWeb extends WebPlugin implements OpenFolderPlugin {
  async pick(options: { defaultPath: string }): Promise<{ value: string }> {
    console.log('ECHO', options);
    return { value: 'no' };
  }
}
