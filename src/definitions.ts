export interface OpenFolderPlugin {
  pick(options: { defaultPath: string }): Promise<{ value: string }>;
}
