import { registerPlugin } from '@capacitor/core';

import type { OpenFolderPlugin } from './definitions';

const OpenFolder = registerPlugin<OpenFolderPlugin>('OpenFolder', {
  web: () => import('./web').then(m => new m.OpenFolderWeb()),
});

export * from './definitions';
export { OpenFolder };
