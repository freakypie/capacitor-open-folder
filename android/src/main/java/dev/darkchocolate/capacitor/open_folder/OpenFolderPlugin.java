package dev.darkchocolate.capacitor.open_folder;

import androidx.activity.result.ActivityResult;
import androidx.documentfile.provider.DocumentFile;

import com.getcapacitor.JSObject;
import com.getcapacitor.JSArray;
import com.getcapacitor.Plugin;
import com.getcapacitor.PluginCall;
import com.getcapacitor.PluginMethod;
import com.getcapacitor.annotation.ActivityCallback;
import com.getcapacitor.annotation.CapacitorPlugin;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.Intent;
import android.net.Uri;
import android.provider.DocumentsContract;

import java.io.InputStream;
import java.io.OutputStream;


@CapacitorPlugin(name = "OpenFolder")
public class OpenFolderPlugin extends Plugin {

  @PluginMethod()
  public void pick(PluginCall call) {
    final String defaultPath = call.getString("defaultPath");
    // Choose a directory using the system's file picker.
    Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT_TREE);

    // Optionally, specify a URI for the directory that should be opened in
    // the system file picker when it loads.
    intent.putExtra(DocumentsContract.EXTRA_INITIAL_URI, defaultPath);

    startActivityForResult(call, intent, "onActivityResult");
  }

  @PluginMethod()
  public void list(PluginCall call) {
    final Uri path = Uri.parse(call.getString("path"));

    Uri returnUri = null;
    try {
      String docId = DocumentsContract.getTreeDocumentId(path);
      returnUri = DocumentsContract.buildChildDocumentsUriUsingTree(path, docId);
    } catch(Exception ex) {
      call.reject(ex.toString());
      return;
    }

    try {
      final DocumentFile directory = DocumentFile.fromTreeUri(getContext(), returnUri);
      JSObject retval = new JSObject();
      retval.put("tree", this.listDirectory(directory));
      call.resolve(retval);
    } catch (IllegalArgumentException ex) {
      call.reject(ex.toString());
    }
  }

  @ActivityCallback()
  public void onActivityResult(PluginCall call, ActivityResult result) {
    if (call == null) {
      return;
    }
    if (result.getResultCode() == Activity.RESULT_OK) {
      // Uri.parse(result.getData().getDataString());
      final Uri uri = result.getData().getData();
      getContext().getContentResolver().takePersistableUriPermission(
        uri, Intent.FLAG_GRANT_WRITE_URI_PERMISSION
      );
      getContext().getContentResolver().takePersistableUriPermission(
        uri, Intent.FLAG_GRANT_READ_URI_PERMISSION
      );

      Uri returnUri = null;
      try {
        String docId = DocumentsContract.getTreeDocumentId(uri);
        returnUri = DocumentsContract.buildChildDocumentsUriUsingTree(uri, docId);
      } catch(Exception ex) {
        System.out.println("error: " + ex.toString());
      }

      DocumentFile directory = DocumentFile.fromTreeUri(getContext(), returnUri);
      JSObject retval = new JSObject();
      retval.put("uri", uri.toString());
      retval.put("tree", listDirectory(directory));
      call.resolve(retval);
    } else {
      call.reject("Failed to pick a directory");
    }
  }

  public JSArray listDirectory(DocumentFile file) {
    JSArray uris = new JSArray();
    for (DocumentFile child: file.listFiles()) {
      JSObject node = new JSObject();
      node.put("name", child.getName());
      node.put("uri", child.getUri().toString());
      if (child.isDirectory()) {
        node.put("type", "directory");
        node.put("children", listDirectory(child));
      } else {
        node.put("type", "file");
      }
      uris.put(node);
    }
    return uris;
  }
}
