# open-folder

allows access to a folder structure

## Install

```bash
npm install open-folder
npx cap sync
```

## API

<docgen-index>

* [`pick(...)`](#pick)

</docgen-index>

<docgen-api>
<!--Update the source file JSDoc comments and rerun docgen to update the docs below-->

### pick(...)

```typescript
pick(options: { defaultPath: string; }) => Promise<{ value: string; }>
```

| Param         | Type                                  |
| ------------- | ------------------------------------- |
| **`options`** | <code>{ defaultPath: string; }</code> |

**Returns:** <code>Promise&lt;{ value: string; }&gt;</code>

--------------------

</docgen-api>
